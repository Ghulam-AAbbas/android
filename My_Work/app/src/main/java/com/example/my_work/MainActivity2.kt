package com.example.my_work

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.widget.Toast
import androidx.core.widget.addTextChangedListener
import kotlinx.android.synthetic.main.activity_main2.*

class MainActivity2 : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main2)
        loginMail()
    }
    fun loginMail(){
        edMail.addTextChangedListener(object : TextWatcher{
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                if (android.util.Patterns.EMAIL_ADDRESS.matcher(edMail.text.toString()).matches()){
                    loginBtn.isEnabled = true
                }
                else{
                    loginBtn.isEnabled = false
                    edMail.error = "Invalid Email"
                }
            }

            override fun afterTextChanged(p0: Editable?) {}

        })
        loginBtn.setOnClickListener{
            edMail.setText("")
            pass.setText("")
            startActivity(Intent(this,SplashScreen::class.java))
            Toast.makeText(this, "Login is Successfully",Toast.LENGTH_SHORT).show()
        }
    }
}